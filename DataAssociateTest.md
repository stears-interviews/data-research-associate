# Stears Data Production Homework

Thank you for your application to Stears!

This assessment is intended for a versatile researcher with experience creating proprietary datasets and a keen eye for detail and quality control. 

We require that you complete all tasks below so we have enough information to assess your current expertise.

#### Submissions

Submit your responses using this Google [form](https://docs.google.com/forms/d/e/1FAIpQLScjjTrFq-CLa8bzTRAHf44sP-ZPMRXA6V11JyLiYhgDBQV9Ig/viewform). 

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment Criteria

Our favourite submissions are -

1. **Clear & simple** - With concise responses to the tasks.
2. **Logical** - Providing clear justifications for your responses.
3. **Show attention to detail** - By ensuring your responses address the tasks outlined with no errors.

Your submission will also impress us by demonstrating one or more of the following -

- **Awareness** of industry trends and how this connects with use cases for datasets
- **Unique skills** in specific areas such as data collection, data analysis & data science

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.

## Assessment

### Scenario

The customers of Stears Data are very interested in data on the financial services industry. To meet their demand, we want to expand our data portfolio to include more datasets on fintech and financial services.

One very popular dataset on Stears Data is [Fintech Lenders: Access and Distribution Channels](https://data.stearsdata.com/data-catalogue/fintech-lenders). This dataset was created through manual research conducted by the team. Customers like it because it provides a nice overview of all the companies who are active in the sector and the differences between their services. Customers have expressed desire for similar datasets that cover a broader range of fintech services, not just lending.

### Your task: Envisioning a new dataset for the team to create. 

Please create a sample of a new dataset that provides an industry overview of payments providers in Africa. Your submission should include:
- An outline of the methodology you would use to create a comprehensive list of the companies that offer payments services in Africa (max 200 words).
- A spreadsheet that mocks-up the final data output. The spreadsheet should include all column headers that you think are important to include in the dataset and five (5) rows of example data.

